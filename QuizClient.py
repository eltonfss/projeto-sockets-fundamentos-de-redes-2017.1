#coding=utf-8
import socket

HOST = '127.0.0.1'     # Endereco IP do Servidor

PORT = 5003          # Porta que o Servidor esta

clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets

socketConfiguration = (HOST, PORT) #cria um array com a configuração do socket IP:PORTA

clientSocket.connect(socketConfiguration) #abrir socket ativo para comunicação com o servidor

print 'Para sair use CTRL+X\n' 

pergunta = clientSocket.recv(1024)

print pergunta

resposta = raw_input() #captura o que usuario digitou

clientSocket.send (resposta) #envia mensagem para o servidor

resultado = clientSocket.recv(1024)

print resultado

clientSocket.close() #fechar socket ativo
