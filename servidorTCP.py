# coding=utf-8

import socket
HOST = '127.0.0.1'     # Endereco IP do Servidor
PORT = 5000            # Porta que o Servidor esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets

orig = (HOST, PORT)  #cria um array com a configuração do socket IP:PORTA

tcp.bind(orig) #abrir socket 

tcp.listen(1) #colocar socket em modo passivo

while True: #para sempre
    
	con, cliente = tcp.accept() #armazena novas conexões na variavel cliente
	print 'Concetado por', cliente
    
	while True: #para sempre
		msg = con.recv(1024) #receber mensagens do cliente
		if not msg: break #se mensagem vazia finaliza recebimento de mensagens
		print cliente, msg #imprime cliente e mensagem
	print 'Finalizando conexao do cliente', cliente
    
	con.close() #finaliza conexão