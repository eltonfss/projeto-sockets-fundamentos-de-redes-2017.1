# coding=utf-8
import socket
import hashlib
import json
import time
import signal
from random import randint

timeout = 1

def timeout_handler(signum, frame):
	print "Timeout alcancado!"
	raise Exception

def ler_opcao():
	print "1 - Estabelecer conexao TCP com Timeout"
	print "2 - Estabelecer conexao TCP"
	print "3 - Transferir video com o uso do protocolo TCP"
	print "4 - Transferir video com o uso do protocolo UDP"
	print "0 - Sair"

	print "Digite a opcao escolhida:"

	opcao = raw_input() #captura o que usuario digitou
	return opcao

HOST = '127.0.0.1'     # Endereco IP do Servidor

PORT_TCP = 5008            # Porta que o Servidor TCP esta
PORT_UDP = 5009            # Porta que o Servidor UDP esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets tcp
udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) #instancia um objeto da api sockets udp

dest_tcp = (HOST, PORT_TCP)  #cria um array com a configuração do socket IP:PORTA
dest_udp = (HOST, PORT_UDP)  #cria um array com a configuração do socket IP:PORTA

signal.signal(signal.SIGALRM, timeout_handler) #api para gerencia de timeout

sequenceNumber = 1
acknowledgmentNumber = 1

tcpActive = False
serverActive = False

opcao = ler_opcao()

while True:

	if(serverActive == False and opcao != '0'):
		try:
			tcp.connect(dest_tcp)
			serverActive = True
		except socket.error:
			print 'Servidor Indisponivel! Tente novamente mais tarde!'
			opcao = "0"

	if(opcao == '1' or opcao == '2'):
	
		tcp.send(opcao)

		header = json.dumps({
			'SYN': '1',
			'sequenceNumber': str(sequenceNumber)
		})

		received = False
		while(received == False):

			#Enviar SYN 1
			headerSize = str(len(header))
			tcp.send(headerSize)
			tcp.send(header)

			try:
				signal.alarm(timeout)

				headerSizeAck = tcp.recv(2)
				ack = tcp.recv(int(headerSizeAck))
				ackJSON = json.loads(ack)	

				print 'Seq' + ackJSON['sequenceNumber']
				print 'ACK' + ackJSON['acknowledgmentNumber']
				print 'SYN' + ackJSON['SYN']
				
				if(ackJSON['acknowledgmentNumber'] > sequenceNumber and ackJSON['SYN'] == '1'):
					acknowledgmentNumber = int(ackJSON['sequenceNumber']) + 1
					sequenceNumber += 1
					received = True
					signal.alarm(0)
			except Exception:
				print "Conexao TCP nao pode ser estabelecida!"
				break

		if received == False:
			opcao = ler_opcao()
			continue
			
		header = json.dumps({
			'SYN': '0',
			'sequenceNumber': str(sequenceNumber),
			'acknowledgmentNumber': str(acknowledgmentNumber)
		})

		headerSize = str(len(header))
		tcp.send(headerSize)
		tcp.send(header)

		sequenceNumber +=1
		tcpActive = True
		print 'Conexao TCP Estabelecida'

  	elif(opcao == '3'):
  	
	  	if(tcpActive == False):
	  		print 'Para enviar o video via TCP e necessario estabelecer a conexao primeiro!'
	 	else:
		  	tcp.send(opcao)

		  	#ler arquivo de video
		  	
			arquivoLido = False
			while arquivoLido == False:
				try:
					print "Digite o nome do arquivo (com extensao):"
					nomeDoArquivo = raw_input()
					arquivo = open(nomeDoArquivo, "r")
					dados = arquivo.read()
					arquivo.close()
					arquivoLido = True
				except IOError:
					print "Arquivo informado nao existe!"

			#segmentar e enviar
			bytes = len(dados)
			tamanhoDoSegmento = 10000
			numeroDeSegmentos= bytes/tamanhoDoSegmento
			#print 'numeroDeSegmentos' + str(numeroDeSegmentos)
			if(bytes%tamanhoDoSegmento):
				numeroDeSegmentos+=1

			for i in range(0, bytes+1, tamanhoDoSegmento):
				
				data = dados[i:i+ tamanhoDoSegmento]

				checksum = hashlib.md5(data).hexdigest()
		
				dataSize = len(data)	

				received = False
				
				while(received == False):
					
					sqNumber = sequenceNumber

					#simulacao de numbero de sequencia incorreto
					if(randint(1,50) == 10):
						print "Alterando Numero de Sequencia!"
						sqNumber += 1
								
					header = json.dumps({
						'checksum': str(checksum),
						'dataSize': str(dataSize),
						'sequenceNumber': str(sqNumber),
						'acknowledgmentNumber': str(acknowledgmentNumber)
					})

					headerSize = str(len(header))

					#simulacao de arquivo corrompido
					body = str(data)
					if(randint(1, 50) == 10):
						print "Alterando corpo da Mensagem!"
						body = body.replace("0","1")

					tcp.send(headerSize)
					tcp.send(header)
					tcp.send(body)
					
					try:
						signal.alarm(timeout)
						headerSizeAck = tcp.recv(2)
						ack = tcp.recv(int(headerSizeAck))
						ackJSON = json.loads(ack)	

						print 'Seq' + ackJSON['sequenceNumber']
						print 'ACK' + ackJSON['acknowledgmentNumber']
						
						if(int(ackJSON['acknowledgmentNumber']) > sequenceNumber and int(ackJSON['sequenceNumber']) == acknowledgmentNumber):
							acknowledgmentNumber += 1
							sequenceNumber += dataSize
							received = True
							signal.alarm(0)
					except Exception:
						print 'Reenviando segmento!'
			
			print 'Arquivo enviado!'
			tcp.send('-1')

	elif(opcao == '4'):
  	
  	  	tcp.send(opcao)

	  	#ler arquivo de video
	  	
		arquivoLido = False
		while arquivoLido == False:
			try:
				print "Digite o nome do arquivo (com extensao):"
				nomeDoArquivo = raw_input()
				arquivo = open(nomeDoArquivo, "r")
				dados = arquivo.read()
				arquivo.close()
				arquivoLido = True
			except IOError:
				print "Arquivo informado nao existe!"

		#segmentar e enviar
		bytes = len(dados)
		tamanhoDoSegmento = 10000
		numeroDeSegmentos= bytes/tamanhoDoSegmento
		#print 'numeroDeSegmentos' + str(numeroDeSegmentos)
		if(bytes%tamanhoDoSegmento):
			numeroDeSegmentos+=1

		for i in range(0, bytes+1, tamanhoDoSegmento):
			
			data = dados[i:i+ tamanhoDoSegmento]

			checksum = hashlib.md5(data).hexdigest()

			dataSize = len(data)	

			header = json.dumps({
				'checksum': str(checksum),
				'dataSize': str(dataSize),
				'sequenceNumber': str(sequenceNumber),
				'acknowledgmentNumber': str(acknowledgmentNumber)
			})

			headerSize = str(len(header))

			received = False
			
			while(received == False):
				
				udp.sendto(headerSize, dest_udp)
				udp.sendto(header, dest_udp)
				udp.sendto(data, dest_udp)

				time.sleep(0.0001)
				
				headerSizeAck = udp.recv(2)
				ack = udp.recv(int(headerSizeAck))
				ackJSON = json.loads(ack)	

				print 'Seq' + ackJSON['sequenceNumber']
				print 'ACK' + ackJSON['acknowledgmentNumber']
				
				if(int(ackJSON['acknowledgmentNumber']) > sequenceNumber and int(ackJSON['sequenceNumber']) == acknowledgmentNumber):
					acknowledgmentNumber += 1
					sequenceNumber += dataSize
					received = True

		print 'Arquivo enviado!'
		udp.sendto('-1',dest_udp)

  	elif(opcao == '0'):
  		if(serverActive == True):
	  		tcp.send(opcao)
		  	tcp.close()
	  		udp.close()
	  	break
  
  	else:
  		print "Opcao invalida! Digite uma das opcoes disponiveis"

	opcao = ler_opcao()



	

