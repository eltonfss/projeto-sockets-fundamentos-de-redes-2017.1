# coding=utf-8

import socket
import json
import hashlib
import time
import os
from random import randint

timeout = 1

HOST = '127.0.0.1'     # Endereco IP do Servidor
PORT_TCP = 5008            # Porta que o Servidor TCP esta
PORT_UDP = 5009            # Porta que o Servidor UDP esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets
udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) #instancia um objeto da api sockets udp

orig_tcp = (HOST, PORT_TCP)  #cria um array com a configuração do socket IP:PORTA
orig_udp = (HOST, PORT_UDP)  #cria um array com a configuração do socket IP:PORTA

tcp.bind(orig_tcp) #abrir socket tcp
udp.bind(orig_udp) #abrit socket udp

tcp.listen(1) #colocar socket em modo passivo

while True: #para sempre
	con, cliente = tcp.accept() #armazena novas conexões na variavel cliente
	sequenceNumber = 1
	acknowledgmentNumber = 1
	while True:
		opcao = con.recv(1)

		if(opcao == '1'):
			#receber SYN 1
			headerSize = con.recv(2)
			header = con.recv(int(headerSize))
			headerJSON = json.loads(header)

			print 'Seq' + headerJSON['sequenceNumber']
			print 'SYN' + headerJSON['SYN']
			acknowledgmentNumber += 1

			time.sleep(timeout)

		elif(opcao == '2'):
			
			#receber SYN 1
			headerSize = con.recv(2)
			header = con.recv(int(headerSize))
			headerJSON = json.loads(header)

			print 'Seq' + headerJSON['sequenceNumber']
			print 'SYN' + headerJSON['SYN']
			
			if(headerJSON['SYN'] == '1'):

				acknowledgmentNumber += 1

				headerAck = json.dumps({
					'SYN': '1',
					'sequenceNumber': str(sequenceNumber),
					'acknowledgmentNumber': str(acknowledgmentNumber)
				})

				received = False
			
				while(received == False):

					#enviar SYN 1
					headerSizeAck = str(len(headerAck))
					con.send(headerSizeAck)
					con.send(headerAck)

					time.sleep(0.0001)

					headerSize = con.recv(2)
					header = con.recv(int(headerSize))
					headerJSON = json.loads(header)	

					print 'Seq' + headerJSON['sequenceNumber']
					print 'ACK' + headerJSON['acknowledgmentNumber']
					print 'SYN' + headerJSON['SYN']
					
					#receber SYN 0
					if(headerJSON['SYN'] == '0' and headerJSON['acknowledgmentNumber'] > sequenceNumber):
						acknowledgmentNumber = int(headerJSON['sequenceNumber']) + 1
						sequenceNumber += 1
						received = True
				
				print 'Conexao TCP estabelecida!'

		elif(opcao == '3'):

			if os.path.isfile("videoOutputTCP") :
				os.remove("videoOutputTCP")

			f = open('videoOutputTCP','wb')

			buffer = dict()

			while True:

				headerSize = con.recv(3) 
				
				if headerSize == '-1':
					f.close()	
					print "Arquivo recebido!"
					break
				
				header = con.recv(int(headerSize))
				
				headerJSON = json.loads(header)
				
				dataSize = headerJSON['dataSize']
				
				data = con.recv(int(dataSize))

				print 'Seq' + headerJSON['sequenceNumber']
				print 'ACK' + headerJSON['acknowledgmentNumber']
				
				if randint(1, 50) == 10: 
						print "Simulando Timeout!"
						time.sleep(timeout)
						continue

				checksum = hashlib.md5(data).hexdigest()

				if(int(headerJSON['sequenceNumber']) != acknowledgmentNumber):
					print "Numero de sequencia incorreto!"
				
				elif(int(headerJSON['sequenceNumber']) == acknowledgmentNumber):

					if(checksum == headerJSON['checksum']):
						
						f.write(data)

						acknowledgmentNumber += int(dataSize);

						ack = json.dumps({
							'sequenceNumber': str(sequenceNumber),
							'acknowledgmentNumber': str(acknowledgmentNumber)
						})	

						con.send(str(len(ack)))
						con.send(ack)

						sequenceNumber += 1	

					else:
						print 'Segmento corrompido!'

		elif(opcao == '4'):

			if os.path.isfile("videoOutputUDP") :
				os.remove("videoOutputUDP")

			f = open('videoOutputUDP','wb')

			buffer = dict()

			while True:

				headerSize, client = udp.recvfrom(3) 
				
				if headerSize == '-1':
					f.close()	
					print "Arquivo recebido!"
					break
				
				header = udp.recv(int(headerSize))
				
				headerJSON = json.loads(header)
				
				dataSize = headerJSON['dataSize']
				
				data = udp.recv(int(dataSize))

				print 'Seq' + headerJSON['sequenceNumber']
				print 'ACK' + headerJSON['acknowledgmentNumber']

				checksum = hashlib.md5(data).hexdigest()

				if(int(headerJSON['sequenceNumber']) > acknowledgmentNumber):
					buffer[headerJSON['sequenceNumber']] = data
					print 'saved in buffer'

				elif(int(headerJSON['sequenceNumber']) == acknowledgmentNumber):

					if(checksum == headerJSON['checksum']):
						f.write(data)
						acknowledgmentNumber += int(dataSize);
					else:
						print 'Checksum did not match!'
				
					
				ack = json.dumps({
					'sequenceNumber': str(sequenceNumber),
					'acknowledgmentNumber': str(acknowledgmentNumber)
				})	

				udp.sendto(str(len(ack)), client)
				udp.sendto(ack, client)

				sequenceNumber += 1	
		elif opcao == "0":
			con.close() #finaliza conexão
			break		
		    
	