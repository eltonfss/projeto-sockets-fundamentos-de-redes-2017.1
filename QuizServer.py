#coding=utf-8

from random import randint
import socket

HOST = ''     # Endereco IP do Servidor (endereço da própria máquina
PORT = 5003   # Porta que o Servidor esta

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets

socketConfiguration = (HOST, PORT)  #cria um array com a configuração do socket IP:PORTA

serverSocket.bind(socketConfiguration) #abrir socket 

serverSocket.listen(1) #colocar socket em modo passivo

print 'Aguardando conexão...'

while True: #para sempre
    
	connection, clientSocket = serverSocket.accept() #armazena novas conexões na variavel cliente
	print 'Conectado por', clientSocket

	
	numeroDeRoteadores = 1000 * randint(0,9)
	atrasoDeProcessamento = 1000 * randint(0,9)
	atrasoDePropagacao = 1000 * randint(0,9)
	atrasoDeTransmissao = 1000 * randint(0,9)
	atrasoFimAFim = numeroDeRoteadores * (atrasoDeProcessamento + atrasoDePropagacao + atrasoDeTransmissao)

    	connection.sendall("Questão 1: Suponha que haja " + str(numeroDeRoteadores) + " roteadores entre a máquina de origem e a de destino. Imagine também que a rede não esteja congestionada (e, portanto, os atrasos de fila sejam desprezíveis), que o atraso de processamento em cada roteador e na máquina de origem seja " + str(atrasoDeProcessamento) + " segundos, que atraso de propagação em cada enlace seja " + str(atrasoDePropagacao) + " segundos e que o atraso de transmissão de meio é " + str(atrasoDeTransmissao) + " segundos. Qual é o atraso fim a fim em segundos?")

	while True: #para sempre
		resposta = connection.recv(1024) #receber mensagens do cliente
		if not resposta: break #se mensagem vazia finaliza recebimento de mensagens
		print clientSocket," respondeu ", resposta #imprime cliente e mensagem
		if(float(resposta) == atrasoFimAFim):
			connection.sendall("Parabéns! Você acertou!")			
			print clientSocket, " acertou!"
		else:
			connection.sendall("Você errou! A resposta correta é " + str(atrasoFimAFim) + " segundos.")
			print clientSocket, " errou!"

	print 'Finalizando conexao do cliente', clientSocket
    
	con.close() #finaliza conexão
