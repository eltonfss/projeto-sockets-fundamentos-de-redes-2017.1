import hashlib
import json
import struct

def estabelecerConexao(tcp):
	tcp.connect(dest) #abrir socket ativo para comunicação com o servidor

def enviarVideo(tcp):

	#ler arquivo de video
	nomeDoArquivo = "video.mp4"
	arquivo = open(nomeDoArquivo, "r")
	dados = arquivo.read()
	arquivo.close()

	#segmentar e enviar
	bytes = len(dados)
	tamanhoDoSegmento = 1024
	numeroDeSegmentos= bytes/tamanhoDoSegmento
	if(bytes%tamanhoDoSegmento):
		numeroDeSegmentos+=1

	for i in range(0, bytes+1, tamanhoDoSegmento):
		
		data = dados[i:i+ tamanhoDoSegmento]

		checksum = hashlib.md5(data).hexdigest()

		# #simulacao de alteracao da mensagem
		# if count == 20:
		# 	data += "23212de"

		dataSize = len(data)	

		header = json.dumps({
			'checksum': str(checksum),
			'dataSize': str(dataSize),
			'sequenceNumber': str(sequenceNumber),
			'acknowledgmentNumber': str(acknowledgmentNumber)
		})

		headerSize = str(len(header))

		received = False
		
		while(received == False):
			
			tcp.send(headerSize)
			tcp.send(header)
			tcp.send(data)

			time.sleep(0.001)
			
			headerSizeAck = tcp.recv(2)
			ack = tcp.recv(int(headerSizeAck))
			ackJSON = json.loads(ack)	

			print 'Seq' + ackJSON['sequenceNumber']
			print 'ACK' + ackJSON['acknowledgmentNumber']
			
			if(ackJSON['acknowledgmentNumber'] > sequenceNumber):
				acknowledgmentNumber = int(ackJSON['sequenceNumber']) + 1
				sequenceNumber += dataSize
				received = True

	tcp.send('-1')