import socket

HOST = '127.0.0.1'     # Endereco IP do Servidor

PORT = 5000            # Porta que o Servidor esta

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #instancia um objeto da api sockets

dest = (HOST, PORT) #cria um array com a configuração do socket IP:PORTA

tcp.connect(dest) #abrir socket ativo para comunicação com o servidor

print 'Para sair use CTRL+X\n' 

msg = raw_input() #captura o que usuario digitou

while msg <> '\x18': #enquanto mensagem diferente de codigo de saida
    tcp.send (msg) #envia mensagem para o servidor
    msg = raw_input() #captura o que o usario digitou novamente

tcp.close() #fechar socket ativo